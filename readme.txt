INTRODUCTION
-------------------------------------------
The Royal Game of Ur is the oldest known board game. It is the precursor to games like Backgammon, Parchisi, and Sorry, just to name a few. It's rules are surprisingly complex and elegant for a game so old, but they are still fairly simple to understand.


RULES
-------------------------------------------
The game starts with two teams, in this game represented by "O" and "X", with six stones each, Like so:

 X X X X X X

< > [ ] [ ] [ ]         < > [ ] [ ]
[ ] [ ] [ ] < > [ ] [ ] [ ] [ ] [ ]
< > [ ] [ ] [ ]         < > [ ] [ ]

 O O O O O O

The first player, O, will start out around the board like so:

 X X X X X X

< > [ ] [ ] [ ]         < > [ ] [ ]
[ ] [ ] [ ] < > [ ] [ ] [ ] [ ] [ ]
< > [ ] [ ] [O]         < > [ ] [ ]
	 <<<<<

 O O O O O

They decide how many spaces to move by rolling 4 tetrahedral dice with two points painted.
The painted pips having a value of 1 and unpainted pips having a value of 0.
The total roll is the sum of all four values.
( Note: This is equivilant to four "coin flips" )

The result is that a player can move anywhere between 0 and 4 spaces on any turn, with the
most common roll being 2.

If player 1, O, rolled a 2 on their first turn, this would be the result:

 X X X X X X

< > [ ] [ ] [ ]         < > [ ] [ ]
[ ] [ ] [ ] < > [ ] [ ] [ ] [ ] [ ]
< > [ ] [O] [ ]         < > [ ] [ ]

 O O O O O

Where as if player 2, X, rolled a 2 on the first turn, this would be the result:

 X X X X X

< > [ ] [X] [ ]         < > [ ] [ ]
[ ] [ ] [ ] < > [ ] [ ] [ ] [ ] [ ]
< > [ ] [ ] [ ]         < > [ ] [ ]

 O O O O O O

Note that X moves from right to left across the upper left row, while O moves right to left across the lower left row.
Once each player reaches the end of the row, they move into the middle row.

A player may choose any stone to make a legal move with their die roll, but the following moves are not legal:

	1. Any move that would move you into a space occupied by a stone you control.

	2. Any move that would move you into an occupied < > space

	3. Any move that would cause you to overshoot the end.


The < > spaces are special in that they have two purposes. They are a safe space, as noted above, but, also, when you
land on them, you get another turn.

After moving down the middle row, player 1, O, moves down into the lower right row and then moves right to left along it to the end.
Player 2, X, moves up into the upper right row, moving right to left along that row until the end.

As stated in rule 3 above, you cannot overshoot the end, which means if O has the following board state:


 X X X X X X

< > [ ] [ ] [ ]         < > [ ] [ ]
[ ] [ ] [ ] < > [ ] [ ] [ ] [ ] [ ]
< > [ ] [ ] [ ]         < > [O] [ ]

 O O O O O

They must roll EXACTLY a 2 to get off, or a 1 to move into the next square. They cannot exit the board on a 3 or greater.

If you land on a square currently occupied by an opponents stone, you knock that stone off, sending it back to the beginning, much like in Backgammon or Sorry.

The goal of the game is to remove all of your tiles before your opponent. The first one to acomplish that is the winner.

Enjoy.
