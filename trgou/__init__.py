__all__ = ['TheRoyalGameOfUr']

__productname__ = 'TheRoyalGameOfUr'
__version__ = "0.0.5"
__copyright__ = "Copyright 2018 BlacKernel & contributors"
__author__ = "BlacKernel"
__author_email__ = "self.onyxus@protonmail.com"
__description__ = "The oldest known board game"
__license__ = "Licenced under GNU GPLv3"
__bigcopyright__ = """%(__productname__)s %(__version__)s
    %(__license__)s""" % locals()

banner = __bigcopyright__

from .game import *
#import .game_tk import *
