# This class represents the game board tiles
class Tile:

    # This contains the coordinates of the tiles
    # For The Royal Game of Ur, this will be a two number list
    # The first number is the player whose homeboard this tile belongs to
    # (1 for player 1)
    # (-1 for player 2)
    # (0 for the middle)
    # The second number is the distance down the path
    # (0..4 for the player's Inner board)
    # (0..7 for the middle)
    # (4..6 for the player's Outer board)
    coord = None

    # This contains the stone in this tile as an object
    stoneInTile = None

    # This is True if and only if this tile is a roll again space
    isRollAgain = False

    # This is True if and only if this tile is the last space in one of the player's outer boards
    isScoringTile = False

    # This contains the tile(s) accesable from this tile as object(s)
    # This will have a length of 1 in most tiles
    # This will have a length of 2 or more on branching tiles
    # This will have a value of None in scoring tiles
    nextTile = None

    # This contains the playerId of the player that this tile starts for
    startingTileFor = None

    # Debug Script (Dumps the guts)
    def debug(self):
        print()
        print("DEBUG: Tile "     + str( self ) )
        print("-----")
        print("coord: "         + str( self.coord ) )
        print("stoneInTile: "    + str( self.stoneInTile ) )
        
        # This displays the playerId and stoneId of the stone if stoneInTile != None
        if self.stoneInTile != None:
            print("\tmyPlayer: " + str( self.stoneInTile.myPlayer ) )
            print("\t'stoneId': "  + str( self.stoneInTile.myPlayer.stones.index( self.stoneInTile ) ) )
        print("isRollAgain: "    + str( self.isRollAgain ) )
        print("isScoringTile: "  + str( self.isScoringTile ) )
        print("nextTile: "      + str( self.nextTile ) )
        print("startingTileFor: " + str( self.startingTileFor ) )
        print()

# This class represents the player's stone tokens
# Which they move around the board
class Stone:
    
    # This contains the player that owns this stone
    myPlayer = None

    # This contains the current tile as an object
    currentTile = None

    # This is true if the stone is still in it's player's storage
    # Or when it has gotten knocked back into it
    isInStorage = True

    # This is true if the stone has left the far side of the board and scored
    hasScored = False

    # Debug Script (Dumps the guts)
    def debug(self):
        print()
        print("DEBUG: Stone "   + str( self ) )
        print("-----")
        print("myPlayer: "      + str( self.myPlayer ) )
        print("currentTile: "   + str( self.currentTile ) )
        print("isInStorage: "   + str( self.isInStorage ) )
        print()


# This class represents the player and is
# mostly used to store a list of the stones each player owns
class Player:

    # This contains a list of the stones (as objects) the player owns
    stones = None

    # Debug Script (Dumps the guts)
    def debug(self):
        print()
        print("DEBUG: Player " + str( self ) )
        print("-----")
        print("stones: " + str( self.stones ) )
        print()


class Turn:

    # This contains a list of all players
    players = []

    # This contains the current player
    currentPlayer = None

    # This contains the number of players
    numberOfPlayers = 0
