#!/usr/bin/env python

# Imports
import time
import sys
import importlib

# Import all  files
from . import classes
from . import functions
from . import variables

def run():
    #Show the title
    functions.showTitle()

    choice = input("Would you like to play against a [H]uman or an [A]I: ")
    if choice == 'H':
        menu( None )

    elif choice == 'A':
        menu( variables.player2 )

    elif choice == 'exit':
        sys.exit()

    else:
        functions.showTitle()
        print("Please enter 'H', 'A', or 'exit'")
        time.sleep(1)
        run()
        

def menu( aiPlayer ):
    
    # Show the title
    functions.showTitle()

    # Show the board
    functions.showTheBoard( variables.theBoard, variables.theTurn )

    # Show the storage (of the current player obv)
    functions.showTheStorage( variables.theTurn )
    

    # Roll the dice
    functions.showTheDice( variables.diceTotal ) 

    # If the current player has no legal moves...
    if not functions.hasLegalMoves( 
            variables.theTurn.currentPlayer,
            variables.theTiles,
            variables.diceTotal
            ):

        # ... tell them so,
        #     roll the dice,
        #     and start a new turn.
        print("No legal moves.")
        time.sleep(2)

        variables.diceTotal = functions.rollTheDice()

        functions.newTurn( variables.theTurn )
        menu( aiPlayer )

    # If it is the AI's turn...
    if aiPlayer == variables.theTurn.currentPlayer:

        # ... have the AI move.
        stoneChoice = functions.doAI(
                aiPlayer, 
                variables.theTurn,
                variables.theTiles,
                variables.diceTotal
                )

    # Otherwise, it is a humans turn...
    else:

        # ... so find out which stone the player wants to move
        stoneChoice = input("Which stone would you like to move: ")

    # Try to convert the input to an integer...
    try:
        stoneChoice = int(stoneChoice)

    # If that doesn't work...
    except:

        # If you typed exit...
        if stoneChoice == 'exit':

            # ... Bail the fuck out.
            sys.exit()
        
        # Otherwise...
        else:

            # ... don't get glad, get Mad.
            print("PLEASE ENTER AN INTEGER!")
            time.sleep(1)
            menu( aiPlayer )

    # If the input isn't within range of the stones...
    if stoneChoice >= len(variables.theTurn.currentPlayer.stones) or stoneChoice < 0:

        # ... don't get glad, get Mad.
        print("PLEASE ENTER A STONE ID!")
        time.sleep(1)
        menu( aiPlayer )

    # If the stone isn't legal...
    # (holy shit the variable calls O_O)
    if not functions.isLegal( 
            variables.theTurn.currentPlayer.stones[ stoneChoice ],
            functions.findTilesAhead(
                variables.theTurn.currentPlayer.stones[ stoneChoice ],
                variables.theTiles,
                variables.diceTotal
                )
            ):

        # ... tell them it isn't legal.
        print("Not a legal move, try again.")
        time.sleep(1)

        # And restart the turn.
        menu( aiPlayer )

    # MOVE THE STONE!!
    functions.moveTheStone(
            variables.theTurn.currentPlayer.stones[ stoneChoice ],
            variables.theTiles,
            variables.diceTotal
            )
    
    # If the stone landed on a roll again space...
    # (sweet human jegus)
    if variables.theTurn.currentPlayer.stones[ stoneChoice ].currentTile.isRollAgain:

        # ... roll the dice and ...
        variables.diceTotal = functions.rollTheDice()


        # ... restart the turn
        menu( aiPlayer )

    # If the stone landed on the scoring tile...
    if variables.theTurn.currentPlayer.stones[ stoneChoice ].currentTile.isScoringTile:

        # ... empty the scoring tile and...
        variables.theTurn.currentPlayer.stones[ stoneChoice ].currentTile.stoneInTile = None
        variables.theTurn.currentPlayer.stones[ stoneChoice ].currentTile = None

        # ... orphan the stone and...
        variables.theTurn.currentPlayer.stones[ stoneChoice ].myPlayer = None

        
        # ... rip the poor stone's innocent little body out of the player's posession
        #     indicating, of course, that the player has scored.
        #     (I WARNED YOU ABOUT THE STAIRS!)
        variables.theTurn.currentPlayer.stones.pop( 

                # (I TOLD YOU DOG!)
                variables.theTurn.currentPlayer.stones.index(

                    # (IT KEEPS HAPENING!)
                    variables.theTurn.currentPlayer.stones[ stoneChoice ]
                    )
                )

        # now THAT'S what I call garbage collection.
        # BI <- That is a cool guy face not smiling.


    # If the current player has no more stones left to move...
    if len( variables.theTurn.currentPlayer.stones ) == 0:

        # Display win screen
        functions.showTitle()
        print("A WINNER IS YOU, PLAYER " + str( variables.theTurn.players.index( variables.theTurn.currentPlayer ) + 1 ) )
        sys.exit()

        

    # Once finished with all the bullshit,
    # roll the dice and
    # advance to the next turn.
    variables.diceTotal = functions.rollTheDice()
    functions.newTurn( variables.theTurn )
    menu( aiPlayer )


## Uncomment this to make this file executable
#menu()
