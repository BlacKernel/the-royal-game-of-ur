# Imports
import time
import sys
import random

# Import trgou classes
import trgou.classes

# This function sets theTile as the
# starting tile for thePlayer
def setStartingTileFor(theTile, thePlayer):
    theTile.startingTileFor = thePlayer

# This function assigns a numberOfStones
# to thePlayer
def assignStones(thePlayer, numberOfStones):

    thePlayer.stones = []

    for i in range(0, numberOfStones):
        stone = trgou.classes.Stone()
        stone.myPlayer = thePlayer
        thePlayer.stones.append(stone)


# This function rotates through the numberOfPlayers
def newTurn(theTurn):
    theTurn.currentPlayer = theTurn.players[ (theTurn.players.index( theTurn.currentPlayer ) + 1) % theTurn.numberOfPlayers ]


# This function knocks theStone off of the tile it's on
def knockOff(theStone):
    theStone.currentTile.stoneInTile = None
    theStone.currentTile = None
    theStone.isInStorage = True

# This functions returns:
# True if the destTile is legal for theStone to move into
# False if it is not legal.

def isLegal(theStone, tilesAhead):

    # <DEBUG LOG>
    #print("LOG: Checking legality...")

    # Check how many scoring tiles there
    # are in front of us.
    
    numberOfScoringTiles = 0
    for tile in tilesAhead:
        if tile.isScoringTile == True:
            numberOfScoringTiles += 1

    # If there are more than 1 scoring tile in our path
    if numberOfScoringTiles > 1:

        # bail!
        return False
    
    # If all is good with the path...
    # ... the destination will be the last tile
    #     in the path
    destTile = tilesAhead[-1]

    # If destTile is empty...
    if destTile.stoneInTile == None:

        # ... it is always legal to move there
        # <DEBUG LOG> #
        #print("--LOG: The tile is empty!")
        return True

    # Otherwise, destTile is occupied
    # If it is also a roll again space...
    if destTile.isRollAgain == True:

        # ... it is never legal to move there
        # <DEBUG LOG> #
        #print("--LOG: Tile is a safe space!")
        return False

    # Otherwise,
    # If the stone occupied in it is friendly...
    if destTile.stoneInTile.myPlayer == theStone.myPlayer:

        # ... it is never legal to move there
        # <DEBUG LOG> #
        #print("--LOG: Friendly stone blocking path!")
        return False

    # Otherwise, it is occupied with a non-friendly...
    # ... and you can move there (to knock them off)
        
    # <DEBUG LOG> #
    #print("--LOG: Can knock player off!")
    return True

# This function is pretty self-explanitory
# It loads 4 random values ( 0 or 1 )
# into a list "dice" and returns the
# sum of all of the values.

def rollTheDice():

    # <DEBUG LOG> #
    #print("LOG: ROLLING THE DICE!")
    dice = []
    for i in range(0, 4):
        dice.append( random.randrange(0,2) )
    
    return sum( dice )

# This function returns a list of tiles
# distance toCheck from theStone on the board.
# theTiles is used, because the tiles are internally linked and
# theBoard variable is entirely for the visuals
def findTilesAhead( theStone, theTiles, toCheck ):
    
    # This stores the list to add the tiles to
    tilesAhead = []

    # and the spacesToCheck in
    # a way that can be itterated
    spacesToCheck = toCheck

    # If theStone is in storage...
    if theStone.isInStorage:

        # ... look through the tiles on the board...
        for tile in theTiles:
            
            # ... for the starting tile...
            if tile.startingTileFor == theStone.myPlayer:

                # ... and add that as the first space to "move"
                tilesAhead.append(tile)

        # Decrament the spaces left to check
        # as we have checked the first tile
        spacesToCheck -= 1

    # Now to check the rest of the spaces:
    # Until there are no more spaces left to check...
    while spacesToCheck > 0:

        # ... we are going to add the next tile ahead
        
        # If theStone hasn't "moved" yet...
        if tilesAhead == []:

            # ... the next tile is after the one theStone is currently on
            nextTile = theStone.currentTile.nextTile

        # Otherwise, we have :moved"
        # and can add the next tile of the last
        # tile we checked
        else:
            nextTile = tilesAhead[-1].nextTile


        # If the nextTile is non-existant...
        if nextTile == None:

            # ... freak out.
            print("ERROR: We have no next tile!")
            return []

        # Otherwise, if we have more than one choice...
        elif type(nextTile) == type({}):

            # ... branch based on the playerId of theStone
            tilesAhead.append(nextTile[theStone.myPlayer])

        # Otherwise, we only have one choice...
        else:

            # ... so, we can just add the tile
            tilesAhead.append(nextTile)

        # Decrement the number of spaces left to check
        # as we checked a space this loop
        spacesToCheck -= 1

    # return the list we've generated
    return tilesAhead

# Find the destination tile of theStone
# in theTiles (theBoard + theScoringTiles), given theRoll
def findDestTile( theStone, theTiles, theRoll ):
    return findTilesAhead( theStone, theTiles, theRoll )[-1]

# Returns a boolian based on if 
# thePlayer has any avalible moves
def hasLegalMoves( thePlayer, theTiles, theRoll ):

    # If theRoll is 0...
    if theRoll == 0:

        # ... there are never any legal moves
        return False

    # Check every stone in thePlayer's stones
    for stone in thePlayer.stones:

        # If the stone has a legal move...
        if isLegal( stone, findTilesAhead( stone, theTiles, theRoll ) ):

            # ... we have at least 1 legal move
            return True




# Moves theStone along theBoard
# by spacesToMove, if legal.
def moveTheStone( theStone, theTiles, spacesToMove):
    
    # Check to see if our turn is pointless...
    if hasLegalMoves( theStone.myPlayer, theTiles, spacesToMove ) == False:
        print("No Legal Moves!")

        # TODO: Have this maybe wait until the current player hits a button?
        #       What about AI?
        src.imports.time.sleep(1)
        return None

    # Find the destination tile
    destTile = findDestTile( theStone, theTiles, spacesToMove )

    # <DEBUG LOG> #
    #print("LOG: destTile = " + str(destTile.debug()) )

    # <DEBUG LOG> #
    #print("LOG: destTile is legal to move to -- " + str( isLegal(theStone, destTile) ))
    # Check if tile is legal
    if isLegal(theStone, findTilesAhead( theStone, theTiles, spacesToMove)) == False:
        return None

    # If the destTile is occupied, but it is legal...
    if destTile.stoneInTile != None:

        # ... knock off the stone in that tile.
        knockOff( destTile.stoneInTile )

    # If theStone is in storage...
    if theStone.isInStorage:

        # ... remove theStone from storage
        theStone.isInStorage = False

    # Otherwise, it has a current tile...
    else:

        # ... and remove theStone from it
        theStone.currentTile.stoneInTile = None
        theStone.currentTile = None

    # Move theStone into destTile
    theStone.currentTile = destTile
    destTile.stoneInTile = theStone

# Have the AI move their stone along the board
def doAI( aiPlayer, theTurn, theTiles, diceTotal):

    # If it is the AI's turn
    if aiPlayer == theTurn.currentPlayer:
        
        # Loop through all of the AI player's stones
        for stone in aiPlayer.stones:

            # If the stone is playable...
            if trgou.functions.isLegal(
                    stone,
                    trgou.functions.findTilesAhead( stone, theTiles, diceTotal )
                    ):
                # ... wait one second ...
                stoneChoice = int(aiPlayer.stones.index(stone))
                time.sleep(1)

                # ... and play the stone
                return stoneChoice
                
        


## VISUALS ##

### TEXT BASED VISUALS ###

Border = "-----------------------------------"

# Visuals for the title
def showTitle():
    print("\x1b[2J\x1b[H")
    print("The Royal Game of Ur")
    print( Border )


# Visuals for rolling the dice
def showTheDice( theSum ):
    print("Dice Total: " + str( theSum ) )
    print( Border )

# Visuals for theBoard on theTurn
def showTheBoard( theBoard, theTurn ):
    ###
    # Prototype Board:
    # -----------------------------
    # |[ ][ ][ ][ ]      [ ][ ][ ]|
    # |[ ][ ][ ][ ][ ][ ][ ][ ][ ]|
    # |[ ][ ][ ][ ]      [ ][ ][ ]|
    # -----------------------------

    # Loop through each row in theBoard...
    for row in theBoard:

        # Populate a temp variable with info from the row
        rowVis = "| "

        # Loop through the cells of the row
        for cell in row:
            
            # If the cell is non-existant...
            if cell == None:

                # ... put in a blank space and move on
                rowVis += "    "
                continue

            # FIXME: This is kinda stupid. Find a better way to do this.
            # If the tile is roll again...
            if cell.isRollAgain == True:

                # ... make the braces used for the cell triangle
                tileBraces = ["<", ">"]

            # Otherwise, it is not a roll again...
            else:

                # ... so, make the braces square
                tileBraces = ["[", "]"]

            # If the tile is empty...
            if cell.stoneInTile == None:

                # ... put in an empty cell and move on
                # FIXME: This is stupid!
                rowVis += tileBraces[0] + " " + tileBraces[1] + " "
                continue

            # [If we are here, the tile is full.] #

            # If the tile is full with the current player's stone...
            if cell.stoneInTile.myPlayer == theTurn.currentPlayer:

                # ... populate the cell with the stoneId of the stone
                # and move on
                # FIXME: This is stupid!
                rowVis += tileBraces[0] + str( cell.stoneInTile.myPlayer.stones.index( cell.stoneInTile ) ) + tileBraces[1] + " "
                continue

            # [If we are here, the tile is full with a stone] #
            # [ not belonging to the current player.        ] #

            # If the stone in the tile is owned by Player 1...
            if cell.stoneInTile.myPlayer == theTurn.players[0]:
                
                # ... put in an "O" and move on
                rowVis += tileBraces[0] + "O" + tileBraces[1] + " "
                continue

            # Otherwise, it is owned by Player 2...
            else:

                # ... put in an "X"
                rowVis += tileBraces[0] + "X" + tileBraces[1] + " "
                continue

        # We have generated the row Visual now
        # So, we can print out the row and loop
        # to the next one.
        rowVis += "|"
        print(rowVis)

    print(Border)




# Visuals for the storage of the current player
def showTheStorage( theTurn ):
     
    # Display whose turn it is
    # ... (in the stupid way) ... 
    # ... (by grabbing the index of the players from theTurn and adding 1 to it before displaying) ...
    print("Player " + str( theTurn.players.index( theTurn.currentPlayer ) + 1 ) + ":")

    # This will get populated with the storage visuals
    storageVis = ""

    # For every stone the current player owns...
    for stone in theTurn.currentPlayer.stones:

        # ... we are going to show
        #     [ ] if the storage space is empty
        #     [#] if the storage space is full,
        #         where # is theTurn.currentPlayer.stones.index( stone )
        #         which is the super stupid way I have to get the stones index here

        
        # If the stone is in storage...
        if stone.isInStorage:

            # <DEBUG LOG #
            #print("--LOG: Stone " + str( theTurn.currentPlayer.stones.index( stone ) ) + " is in storage!")

            # ... show it's index in a cell.
            storageVis += "[" + str( theTurn.currentPlayer.stones.index( stone ) ) + "] "

            # <DEBUG LOG> #
            #print("--LOG: Current storageVis -- " + storageVis)

        # Otherwise, it is not in storage...
        else:

            # <DEBUG LOG> #
            #print("--LOG: Stone " + str( theTurn.currentPlayer.stones.index( stone ) ) + "is NOT in storage!")

            # ... so, show an empty cell.
            storageVis += "[ ] "

            # <DEBUG LOG> #
            #print("--LOG: Current storageVis -- " + storageVis)

    # Show the storage visuals
    print(storageVis)
    print(Border)
