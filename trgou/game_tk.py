# Import all src files
import time
import random
import sys

import trgou.classes as cls
import trgou.functions as func
import trgou.variables as var

# Import tkinter
from tkinter import *


# Instansiate the Tk object
root = Tk()

## Create Main Window ##

# Window Title
root.title( "The Royal Game of Ur" )

# Make the board
board = Canvas( root, height=300, width=800 )
board.pack()

## NOTE: Normal squares will be blue
##       Roll again squares will be green

# Create player1's home board
# 5 and 6 are skipped, since there are
# no tiles in those spaces

# The if statement there says:
# "If i is 1 or 7 [That is, if i % 6 = 1]...
#  ... Make that square green"
for i in (1, 2, 3, 4, 7, 8):
    board.create_rectangle( (i-1)*100,   0, i*100, 100, fill=( 'blue' if (i%6) != 1 else 'green' ) )

# Create the middle board
for i in range( 1, 9 ):
    board.create_rectangle( (i-1)*100, 100, i*100, 200, fill=( 'blue' if i != 4 else 'green' ) )

# Create player2's home board
# 5 and 6 are skipped, since there are
# no tiles in those spaces

# The if statement there reads the same as
# for player1
for i in (1, 2, 3, 4, 7, 8):
    board.create_rectangle( (i-1)*100, 200, i*100, 300, fill=( 'blue' if (i%6) != 1 else 'green' ) )

# Create player1's storage

playerStorage1 = Canvas( root, height=100, width=500 )
playerStorage1.pack( side='left' )

# Instantiate player1 stones

stone1 = PhotoImage( file="player1_stone.png" )

for i in range( 0, 5 ):
    playerStorage1.create_image( (i*100)+50, 50, image=stone1 )

# Create player2's storage

playerStorage2 = Canvas( root, height=100, width=500 )
playerStorage2.pack( side='right' )

# Instantiate player2 stones

stone2 = PhotoImage( file="player2_stone.png" )

for i in range( 0, 5):
    playerStorage2.create_image( (i*100)+50, 50, image=stone2 )

mainloop()
