# Import trgou classes and functions
import trgou.classes
import trgou.functions

## Instantiatie Players ##

# Player 1
player1 = trgou.classes.Player()
player1.playerId = 0
trgou.functions.assignStones( player1, 6 )

# Player 2
player2 = trgou.classes.Player()
player2.playerId = 1
trgou.functions.assignStones( player2, 6 )

### Instantiate the Board

## Instantiate the tiles for the Player 1 home board
p1_Inner0 = trgou.classes.Tile()
p1_Inner1 = trgou.classes.Tile()
p1_Inner2 = trgou.classes.Tile()
p1_Inner3 = trgou.classes.Tile()

p1_Outer0 = trgou.classes.Tile()
p1_Outer1 = trgou.classes.Tile()

p1_ScoringTile = trgou.classes.Tile()

## Link up Player 1 home board

# Set up starting tile
trgou.functions.setStartingTileFor( p1_Inner0, player1 )

# Link up the nextTile's appropriately
# 0 -> 1 -> 2 -> 3
p1_Inner0.nextTile = p1_Inner1
p1_Inner1.nextTile = p1_Inner2
p1_Inner2.nextTile = p1_Inner3

p1_Outer0.nextTile = p1_Outer1
p1_Outer1.nextTile = p1_ScoringTile

# The Scoring tile goes to itself to stop wierd things from happening there.
p1_ScoringTile.nextTile = p1_ScoringTile

## Set up special properties of Player 1 home board
p1_Inner3.isRollAgain = True
p1_Outer1.isRollAgain = True

p1_ScoringTile.isScoringTile = True

## Group into corrisponding row
p1_HomeBoard = [
        p1_Inner3,
        p1_Inner2,
        p1_Inner1,
        p1_Inner0,
        
        None, # These "None" spaces are the
        None, # two blank spaces on TRGoU board

        p1_Outer1,
        p1_Outer0
        ]

## Instantiate the tiles for Player 2's home board
p2_Inner0 = trgou.classes.Tile()
p2_Inner1 = trgou.classes.Tile()
p2_Inner2 = trgou.classes.Tile()
p2_Inner3 = trgou.classes.Tile()

p2_Outer0 = trgou.classes.Tile()
p2_Outer1 = trgou.classes.Tile()

p2_ScoringTile = trgou.classes.Tile()

## Link up Player 2's home board

# Set the starting tile for Player 2
trgou.functions.setStartingTileFor( p2_Inner0, player2 )

# Link up the rest of the home board
# 0 -> 1 -> 2 -> 3
p2_Inner0.nextTile = p2_Inner1
p2_Inner1.nextTile = p2_Inner2
p2_Inner2.nextTile = p2_Inner3

p2_Outer0.nextTile = p2_Outer1
p2_Outer1.nextTile = p2_ScoringTile

# See p1's board for why this
p2_ScoringTile.nextTile = p2_ScoringTile

## Set up special properties of Player 2's home board
p2_Inner3.isRollAgain = True
p2_Outer1.isRollAgain = True

p2_ScoringTile.isScoringTile = True

## Group into corisponding row
p2_HomeBoard = [
        p2_Inner3,
        p2_Inner2,
        p2_Inner1,
        p2_Inner0,

        None, # See above (in Player 1's home board)
        None, # for why these Nones are important

        p2_Outer1,
        p2_Outer0
        ]

## Instantiate the tiles for the middle board
middle0 = trgou.classes.Tile()
middle1 = trgou.classes.Tile()
middle2 = trgou.classes.Tile()
middle3 = trgou.classes.Tile()
middle4 = trgou.classes.Tile()
middle5 = trgou.classes.Tile()
middle6 = trgou.classes.Tile()
middle7 = trgou.classes.Tile()

## Link up middle board

# At the end of their inner boards
# both players move into the middle
p1_Inner3.nextTile = middle0
p2_Inner3.nextTile = middle0

# Link up the bulk of the middle row accordingly
# 0 -> 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7
middle0.nextTile = middle1
middle1.nextTile = middle2
middle2.nextTile = middle3
middle3.nextTile = middle4
middle4.nextTile = middle5
middle5.nextTile = middle6
middle6.nextTile = middle7

# middle7 branches into the two player's outer boards
middle7.nextTile = {
        player1 : p1_Outer0,
        player2 : p2_Outer0
        }

## Set up special properties of middle board
middle3.isRollAgain = True

## Group into corrisponding row
middleBoard = [
        middle0,
        middle1,
        middle2,
        middle3,
        middle4,
        middle5,
        middle6,
        middle7
        ]

## Group rows into the board
theBoard = [
        p1_HomeBoard,
        middleBoard,
        p2_HomeBoard,
        ]


## Make an unordered list of all tiles to itterate though
theTiles = []

# Add all of the player 1's home board
# to the aggragite list of tiles
for tile in p1_HomeBoard:

    # If the tile is a placeholder None...
    if tile == None:
        
        # ... don't add it, just move on
        continue

    theTiles.append(tile)

# Add all of the player 2's home board
# to the aggragite list of tiles
for tile in p2_HomeBoard:

    # If the tile is a placeholder None...
    if tile == None:

        # ... don't add it, just move on
        continue

    theTiles.append(tile)

# Add all of the middle tiles
# to the aggragite list of tiles
for tile in middleBoard:
    theTiles.append(tile)

# Add the scoring tiles
# to the aggragite list of tiles
theTiles.append(p1_ScoringTile)
theTiles.append(p2_ScoringTile)

## Initialize the turn
theTurn = trgou.classes.Turn()

# Set the players of the turn
theTurn.players = [
        player1,
        player2
        ]

# Initialize the turn to player1's turn
theTurn.currentPlayer = player1
theTurn.numberOfPlayers = 2

# Initialize first die roll
diceTotal = trgou.functions.rollTheDice()
