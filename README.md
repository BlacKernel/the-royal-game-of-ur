# The Royal Game of Ur
### by: BlacKernel
### Licenced under GPLv3

---

***History:***
The Royal Game of Ur is the oldest board game known to humanity. It was found carved onto the floors by the doorways of temples, where the gaurds would presumably play when they were supposed to be doing their job. (Much like you, at the moment.) This game is also the precursor to games such as Seket and Backgammon.

---

***Set up:***
Players start out with a **BOARD**, laid out in the following way:

```python
---------------------------------
|< > [ ] [ ] [ ]         < > [ ]|
|[ ] [ ] [ ] < > [ ] [ ] [ ] [ ]|
|< > [ ] [ ] [ ]         < > [ ]|
---------------------------------
```
Players should also have **4d2**, to determine movement.

Each player starts with 6 stones, which should be a checker roughly the size of one of the tiles. One player gets **WHITE** stones and the other gets **BLACK** stones. At the start of the game, players have their stones in front of them in their **STORAGE**.

---

***Turn:***
At the start of a players turn, they roll **4d2**, subtract **ONE** from each of the dice, and total their value. This total is their **MOVEMENT VALUE**.

A player may choose a stone to move along the board.

If it is the top player's turn, they may move their stone along this path:

```python
---------------------------------
|<v> [<] [<] [<]         <<> [<]|
|[>] [>] [>] <>> [>] [>] [>] [^]|
|< > [ ] [ ] [ ]         < > [ ]|
---------------------------------
```

While the bottom player moves along this path:


```python
---------------------------------
|< > [ ] [ ] [ ]         < > [ ]|
|[>] [>] [>] <>> [>] [>] [>] [v]|
|<^> [<] [<] [<]         <<> [<]|
---------------------------------
```

If a player lands on a tile marked with `< >`, that player may take another turn, starting with the dice again.

If there is an opponent's stone in the tile that your stone moves into, the opponent's stone is sent back to that player's **STORAGE**.

If a stone is on a tile marked with `< >`, that stone cannot be sent back to storage and, therefore, is not a valid space for a stone to move onto while occupied.

In order to move off the board, into that player's **HOME**, a stone must be able to land **EXACTLY ONE** space after the final space of the board.

A player may not split thier **MOVEMENT VALUE** across more than one stone.

If a player has a **MOVEMENT VALUE** of **ZERO**, their turn is skipped and the next person starts their turn.

---

***Goal:***
The goal of The Royal Game of Ur is to move all of your stones from your **STORAGE**, across the board, and into your **HOME**. The first player to do so, is the winner.
