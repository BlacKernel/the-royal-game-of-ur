#!/usr/bin/env python

from setuptools import setup, find_packages
import trgou.main

setup(name = "the-royal-game-of-ur",
        version = "0.0.5",
        description = "The oldest board game.",
        author = "BlacKernel",
        author_email = "self.onyxus@protonmail.com",
        packages = find_packages( exclude=('tests', 'docs') ),
        entry_points = {
            'console_scripts' : [ 'trgou = trgou.main:menu' ]

        }
)
